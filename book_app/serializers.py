from rest_framework.serializers import ModelSerializer

from book_app.models import Book, BookShelf


class BookSerializer(ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'


class BookShelfSerializer(ModelSerializer):
    class Meta:
        model = BookShelf
        fields = '__all__'
