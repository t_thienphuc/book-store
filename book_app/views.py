# Create your views here.
from rest_framework.viewsets import ModelViewSet

from book_app.models import BookShelf, TextBook
from book_app.serializers import BookSerializer, BookShelfSerializer


class ShelfViewSet(ModelViewSet):
    queryset = BookShelf.objects.all()
    serializer_class = BookShelfSerializer


class BookViewSet(ModelViewSet):
    queryset = TextBook.objects.all()
    serializer_class = BookSerializer
