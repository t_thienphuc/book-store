from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class BookShelf(models.Model):
    color = models.CharField(max_length=10)
    book_count = models.IntegerField(default=0)

    def __str__(self):
        return self.color


class Book(models.Model):
    title = models.CharField(max_length=20)
    price = models.IntegerField()
    shelf = models.ForeignKey(BookShelf, on_delete=models.CASCADE)
    is_active = True

    class Meta:
        abstract = True


class TextbookMixin(models.Model):
    grade = models.CharField(max_length=10)

    class Meta:
        abstract = True


class TextBook(Book, TextbookMixin):
    is_active = False
    title = 'Ngu van 12'
