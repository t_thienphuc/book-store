from django.contrib.auth import authenticate
# Create your views here.
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from user_app.models import Account
from user_app.serializers import AccountSerializer, LoginSerializer


class AccountViewSet(ModelViewSet):
    # permission_classes = [IsAuthenticated]
    # authentication_classes = [TokenAuthentication]
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get_queryset(self):
        print(self.request.user)
        if self.request.user.is_anonymous:
            return self.queryset.none()
        if self.request.user.is_staff:
            return self.queryset
        return self.queryset.filter(id=self.request.user)


class AuthViewSet(GenericViewSet):
    serializer_class = AccountSerializer

    @action(detail=False, methods=['post'])
    def login(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        username = serializer.validated_data.get('username')
        password = serializer.validated_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is None:
            raise AuthenticationFailed
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            "token": token.key,
            "user": self.get_serializer(user).data
        })

